class Item {
  int id;
  String name;
  String description;
  String image;
  double rating;
  int ratingCount;
  double price;
  int color;
  List<String> vitamins;
  List<String> ingrediants;

  Item(
      {required this.id,
      required this.name,
      required this.description,
      required this.image,
      required this.rating,
      required this.ratingCount,
      required this.price,
      required this.color,
      required this.vitamins,
      required this.ingrediants});
}

List<Item> demoItems = [
  Item(
      id: 1,
      name: 'Fruit soup',
      description:
          "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
      image: 'assets/images/1.png',
      rating: 4.5,
      ratingCount: 565,
      price: 3.50,
      color: 0xFFFFE3E3,
      vitamins: [
        'Vitamin A',
        'Vitamin C',
        'Vitamin K'
      ],
      ingrediants: [
        'assets/icons/ingredient1.svg',
        'assets/icons/ingredient2.svg',
        'assets/icons/ingredient3.svg',
        'assets/icons/ingredient4.svg'
      ]),
  Item(
      id: 2,
      name: 'Salad',
      description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
      image: 'assets/images/2.png',
      rating: 4.5,
      ratingCount: 465,
      price: 2.0,
      color: 0xFF80985C,
      vitamins: [
        'Vitamin A',
        'Vitamin C',
        'Vitamin K'
      ],
      ingrediants: [
        'assets/icons/ingredient1.svg',
        'assets/icons/ingredient2.svg',
        'assets/icons/ingredient3.svg',
        'assets/icons/ingredient4.svg'
      ]),
  Item(
      id: 3,
      name: 'Lasaña',
      description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
      image: 'assets/images/3.png',
      rating: 4.5,
      ratingCount: 565,
      price: 150.00,
      color: 0xFF253B4A,
      vitamins: [
        'Vitamin A',
        'Vitamin C',
        'Vitamin K'
      ],
      ingrediants: [
        'assets/icons/ingredient1.svg',
        'assets/icons/ingredient2.svg',
        'assets/icons/ingredient3.svg',
        'assets/icons/ingredient4.svg'
      ]),
  Item(
      id: 4,
      name: 'Burger',
      description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
      image: 'assets/images/4.png',
      rating: 4.5,
      ratingCount: 165,
      price: 6.5,
      color: 0xFFB79161,
      vitamins: [
        'Vitamin A',
        'Vitamin C',
        'Vitamin K'
      ],
      ingrediants: [
        'assets/icons/ingredient1.svg',
        'assets/icons/ingredient2.svg',
        'assets/icons/ingredient3.svg',
        'assets/icons/ingredient4.svg'
      ]),
  Item(
      id: 5,
      name: 'Kimchi',
      description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
      image: 'assets/images/5.png',
      rating: 4.5,
      ratingCount: 165,
      price: 19.5,
      color: 0xFFFFE087,
      vitamins: [
        'Vitamin A',
        'Vitamin C',
        'Vitamin K'
      ],
      ingrediants: [
        'assets/icons/ingredient1.svg',
        'assets/icons/ingredient2.svg',
        'assets/icons/ingredient3.svg',
        'assets/icons/ingredient4.svg'
      ]),
  Item(
      id: 6,
      name: 'English breakfast',
      description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
      image: 'assets/images/6.png',
      rating: 4.5,
      ratingCount: 165,
      price: 20.5,
      color: 0xFFE55D4C,
      vitamins: [
        'Vitamin A',
        'Vitamin C',
        'Vitamin K'
      ],
      ingrediants: [
        'assets/icons/ingredient1.svg',
        'assets/icons/ingredient2.svg',
        'assets/icons/ingredient3.svg',
        'assets/icons/ingredient4.svg'
      ]),
];
