import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:food_app/Components/app_bar.dart';
import 'package:food_app/Models/Item.dart';
import 'package:food_app/constants.dart';

import 'components/buy.dart';
import 'components/ingredients.dart';
import 'components/qty_counter.dart';
import 'components/title_bar.dart';
import 'components/vitamins.dart';

class DetailsScreen extends StatelessWidget {
  final Item item;

  const DetailsScreen({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: buildAppBar(
        context,
        title: 'Detail',
        actions: [
          IconButton(
            icon: SvgPicture.asset('assets/icons/dots.svg'),
            onPressed: () {},
          ),
        ],
        leading: Transform.translate(
          offset: Offset(kDefaultPadding * 0.5, 0),
          child: IconButton(
            icon: SvgPicture.asset('assets/icons/back.svg'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
      ),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            margin: EdgeInsets.only(top: size.height * 0.2),
            padding: EdgeInsets.only(top: size.height * 0.15),
            height: size.height * 0.8,
            decoration: BoxDecoration(
              color: Color(0xFF003D6C),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(kDefaultPadding * 5),
                topRight: Radius.circular(kDefaultPadding * 5),
              ),
            ),
            child: SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(
                kDefaultPadding,
                0,
                kDefaultPadding,
                kDefaultPadding,
              ),
              child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TitleBar(item: item),
                    SizedBox(
                      height: kDefaultPadding,
                    ),
                    QtyCounter(),
                    SizedBox(
                      height: kDefaultPadding,
                    ),
                    Text(
                      item.description,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14.0,
                      ),
                    ),
                    SizedBox(
                      height: kDefaultPadding,
                    ),
                    Vitamins(item: item),
                    SizedBox(
                      height: kDefaultPadding,
                    ),
                    Text(
                      'Ingredients',
                      style: TextStyle(fontSize: 18.0, color: Colors.white),
                    ),
                    SizedBox(
                      height: kDefaultPadding,
                    ),
                    Ingredients(item: item),
                    SizedBox(
                      height: kDefaultPadding,
                    ),
                    Buy(item: item),
                  ],
                ),
              ),
            ),
          ),
          Hero(
            tag: item.id,
            child: Image.asset(
              item.image,
              height: size.height * 0.35,
              fit: BoxFit.fitHeight,
            ),
          ),
        ],
      ),
    );
  }
}
