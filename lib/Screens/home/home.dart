import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:food_app/Components/app_bar.dart';
import 'package:food_app/Components/bottom_nav.dart';
import 'package:food_app/Models/Item.dart';
import 'package:food_app/constants.dart';

import 'components/categories.dart';
import 'components/filter_button.dart';
import 'components/item_card.dart';
import 'components/search_bar.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        context,
        title: '',
        actions: [
          IconButton(
            icon: SvgPicture.asset('assets/icons/notification.svg'),
            onPressed: () {},
          ),
        ],
        leading: Transform.translate(
          offset: Offset(kDefaultPadding * 0.5, 0),
          child: IconButton(
            icon: Image.asset('assets/images/user.png'),
            onPressed: () {},
          ),
        ),
      ),
      bottomNavigationBar: BottomNavBar(),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(kDefaultPadding),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Find the Best\nHealth for you.',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: kDefaultPadding,
              ),
              SearchBar(),
              Row(
                children: [
                  FilterButton(),
                  Expanded(
                    child: Categories(),
                  ),
                ],
              ),
              SizedBox(
                height: kDefaultPadding,
              ),
              Text(
                'Popular',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                ),
              ),
              SizedBox(
                height: kDefaultPadding,
              ),
              StaggeredGridView.countBuilder(
                crossAxisCount: 2,
                itemCount: demoItems.length,
                crossAxisSpacing: 20.0,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return ItemCard(item: demoItems[index], index: index,);
                },
                staggeredTileBuilder: (index) => StaggeredTile.fit(1),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


